<?php

class Order
{

    /**
     * Save order
     * @param $userName
     * @param $userPhone
     * @param $userComment
     * @param $userId
     * @param $products
     * @return bool
     */

    public static function save($userName, $userPhone, $userComment, $userId, $products)
    {
        $products = json_encode($products);

        $db = Db::getConnection();

        $sql = 'INSERT INTO product_order (user_name, user_phone, user_comment, user_id, products) '
            . 'VALUES (:user_name, :user_phone, :user_comment, :user_id, :products)';

        $result = $db->prepare($sql);
        $result->bindValue(':user_name', $userName);
        $result->bindValue(':user_phone', $userPhone);
        $result->bindValue(':user_comment', $userComment);
        $result->bindValue(':user_id', $userId);
        $result->bindValue(':products', $products);

        return $result->execute();
    }

}