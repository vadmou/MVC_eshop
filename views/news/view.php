<!DOCTYPE html>
<html>
<head>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Одна новость</title>

    <link href="/template/css/style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div>
    <div>
        <div>
            <h2><a href='/news/<?php echo $newsItem['id'] ;?>'><?php echo $newsItem['title'].' # '.$newsItem['id'];?></a></h2>
            <p>Posted by <a href="#"><?php echo $newsItem['author_name'];?></a> on <?php echo $newsItem['date'];?>
                 <a href='/news/' class="permalink"> Back to HomePage</a></p>
            <div>
                <p><img src="/template/images/pic01.jpg" width="200" height="70" alt="" /></p>
                <p><?php echo $newsItem['short_content'];?></p>
            </div>
        </div>
        <p><a href='/news/' class="permalink"> Back to HomePage</a></p>
        <div style="clear: both;">&nbsp;</div>
    </div>
</body>
</html>